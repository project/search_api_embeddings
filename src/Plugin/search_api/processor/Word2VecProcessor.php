<?php

namespace Drupal\search_api_embeddings\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Drupal\search_api\Utility\DataTypeHelperInterface;
use PHPW2V\Word2Vec;

/**
 * Produce word embeddings using Word2vec for natural language processing.
 *
 * @SearchApiProcessor(
 *   id = "word2vec",
 *   label = @Translation("Word2VecProcessor"),
 *   description = @Translation("Produce word embeddings using Word2vec, a two-layer neural network for natural language processing."),
 *   stages = {
 *     "pre_index_save" = 0,
 *     "preprocess_index" = -15,
 *     "preprocess_query" = -15,
 *   }
 * )
 */
class Word2VecProcessor extends FieldsProcessorPluginBase {

  /**
   * The data type helper.
   *
   * @var \Drupal\search_api\Utility\DataTypeHelperInterface|null
   */
  protected $dataTypeHelper;

  /**
   * Retrieves the data type helper.
   *
   * @return \Drupal\search_api\Utility\DataTypeHelperInterface
   *   The data type helper.
   */
  public function getDataTypeHelper() {
    return $this->dataTypeHelper ?: \Drupal::service('search_api.data_type_helper');
  }

  /**
   * Sets the data type helper.
   *
   * @param \Drupal\search_api\Utility\DataTypeHelperInterface $data_type_helper
   *   The new data type helper.
   *
   * @return $this
   */
  public function setDataTypeHelper(DataTypeHelperInterface $data_type_helper) {
    $this->dataTypeHelper = $data_type_helper;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    $configuration += [
      'dimensions' => 5,
      'sampling' => NULL,
      'min_word_count' => 2,
      'alpha' => 0.01,
      'window' => 2,
      'epochs' => 10,
      'subsample' => 1e-3,
    ];

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['dimensions'] = [
      '#type' => 'number',
      '#title' => $this->t('Vector dimension size'),
      '#default_value' => $this->configuration['dimensions'],
      '#description' => $this->t('Dimensions must be greater than 4.'),
      '#min' => '5',
      '#required' => TRUE,
    ];

    $form['sampling'] = [
      '#type' => 'select',
      '#title' => $this->t('Softmax Approximator'),
      '#options' => [
        'NegativeSampling' => 'NegativeSampling',
        'HierarchicalSoftmax' => 'HierarchicalSoftmax',
      ],
      '#default_value' => $this->configuration['sampling'],
      '#description' => $this->t("The Softmax approximation sampling algorithm."),
      '#required' => TRUE,
    ];

    $form['min_word_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum word count'),
      '#default_value' => $this->configuration['min_word_count'],
      '#description' => $this->t('Minimum word count must be greater than 0.'),
      '#min' => '1',
      '#required' => TRUE,
    ];

    $form['window'] = [
      '#type' => 'number',
      '#title' => $this->t('Window for skip-gram'),
      '#default_value' => $this->configuration['window'],
      '#description' => $this->t('Window must be between 1 and 5.'),
      '#min' => '1',
      '#max' => '5',
      '#required' => TRUE,
    ];

    $form['epochs'] = [
      '#type' => 'number',
      '#title' => $this->t('How many epochs to run'),
      '#default_value' => $this->configuration['epochs'],
      '#description' => $this->t('Number of epochs must be greater than 0.'),
      '#min' => '1',
      '#required' => TRUE,
    ];

    $form['alpha'] = [
      '#type' => 'number',
      '#title' => $this->t('Learning rate'),
      '#default_value' => $this->configuration['alpha'],
      '#description' => $this->t('Alpha must be greater than 0.'),
      '#step' => '.01',
      '#required' => TRUE,
    ];

    $form['subsample'] = [
      '#type' => 'number',
      '#title' => $this->t('Subsampling rate'),
      '#default_value' => $this->configuration['subsample'],
      '#description' => $this->t('Sample rate must be 0 or greater.'),
      '#step' => '.001',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    $items = [];

    if (is_array($value)) {
      $word2vec = $this->train($value);

      foreach ($value as $item) {
        $vector = $word2vec->embedWord($item);

        $items[] = [
          'item' => $item,
          'vector' => $vector->asArray(),
        ];
      }
    }

    $value = $items;
  }

  /**
   * Iterate through a sentences array and generate all respective word vectors.
   *
   * @param string[] $sentences
   *   The sentences to train on.
   *
   * @return \PHPW2V\Word2Vec
   *   The trained word2vec model.
   */
  private function train(array $sentences) {
    $sampling_class = $this->configuration['sampling'];

    $word2vec = new Word2Vec(
      $this->configuration['dimensions'],
      new $sampling_class(),
      $this->configuration['window'],
      $this->configuration['subsample'],
      $this->configuration['alpha'],
      $this->configuration['epochs'],
      $this->configuration['min_word_count'],
    );

    $word2vec->train($sentences);
    $word2vec->save('temporary://search_api_embeddings_word2vec_' . time());

    return $word2vec;
  }

}
