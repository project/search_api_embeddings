<?php

namespace Drupal\search_api_embeddings\Plugin\search_api\data_type;

use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides a embedding data type.
 *
 * @SearchApiDataType(
 *   id = "embedding",
 *   label = @Translation("Embedding"),
 *   description = @Translation("Embeddings data type."),
 *   default = "true"
 * )
 */
class Embedding extends DataTypePluginBase {}
