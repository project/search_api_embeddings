# Search API Embeddings

## INTRODUCTION

Collection of Search API Processors to generate embeddings.

The module also contains useful tools like Chunkers and text summarizers.

## REQUIREMENTS

1. [Search API](https://www.drupal.org/project/search_api) module.
2. [PHPW2V](https://github.com/SpinettaInc/PHPW2V), PHP implementation of the 
Word2Vec library.

## INSTALLATION

The module can be installed via the
[standard Drupal installation process](https://drupal.org/node/1897420).

## CONFIGURATION

See [Search API module's README](https://www.drupal.org/node/2852816) for
instructions.
